var array1 = ['a', 'b', 'c'];

array1.forEach(function(element) {
  console.log(element);
});

Array.prototype.myForEach = function (callback){
    for (i of this){
      callback(i)
    }
   }
   
   array1.myForEach(function(element){
    console.log(element);
   })

//map()
   var array2 = [1, 4, 9, 16];
   const map1 = array2.map(x => x * 2);

   console.log(map1)
   
   Array.prototype.myMap = function (callback) {
       let newArray = []
       for (i of this){
           newArray.push(callback(i))
       }
       return newArray
   }
   const map2 = array2.map(x => x * 2);

   console.log(map2)

   //some()

   var mapArray = [1, 3]
  function isEven(number) {
      return number % 2 === 0
  }

  Array.prototype.mySome = function (callback) {

    let output = false
    for(i of this){
        if(callback(i)){
            output = true
        }
    }
      return output
  }
  console.log(mapArray.mySome(isEven))

  //find()

  var array3 = [6, 12, 8, 130, 44];

  var found = array3.find(function(element) {
      return element > 10;
  });

  Array.prototype.myFind = function (callback) {
      for(i of this){
        if(callback(i)){
            return i
        }
  }
  return false 
}
console.log(array3.myFind(isEven))

//findIndex()



  array3.findIndex(function(element) {
      return element > 10;
  });


  Array.prototype.myFindIndex = function (callback) {
      for(i of this){
        if(callback(this[i])){
            return i
        }
  }
  return '-1'
}
console.log(array3.findIndex(isEven))

//every()

Array.prototype.myFindEvery = function (callback) {
    for (let value of this){
        if(!callback(value)) {
            return false
        }
    }
    return true 
}
console.log(array3.myFindEvery(isEven))

//filter()

Array.prototype.myFilter = function (callback) {
    let newArray = []

    for (let value of this) {
        if( callback(value)) {
            newArray.push(value)
        }
            return newArray
        }
    }
console.log(array3)
console.log(array3.myFilter(isEven))
 
console.log(array3.myFilter((value) => {
    return value > 12
}))



